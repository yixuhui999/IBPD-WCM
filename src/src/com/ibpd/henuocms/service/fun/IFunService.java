package com.ibpd.henuocms.service.fun;

import com.ibpd.dao.impl.IBaseService;
import com.ibpd.henuocms.entity.FunEntity;

public interface IFunService extends IBaseService<FunEntity> {
	void init();
}
 