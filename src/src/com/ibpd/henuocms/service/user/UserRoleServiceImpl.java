package com.ibpd.henuocms.service.user;

import java.util.ArrayList;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ibpd.dao.impl.BaseServiceImpl;
import com.ibpd.henuocms.entity.UserRoleEntity;
@Transactional
@Service("userRoleService")
public class UserRoleServiceImpl extends BaseServiceImpl<UserRoleEntity> implements IUserRoleService {
	public UserRoleServiceImpl(){
		super();
		this.tableName="UserRoleEntity";
		this.currentClass=UserRoleEntity.class;
		this.initOK();
	} 

	public List<UserRoleEntity> getListByRoleId(Long roleId) {
		return getList("from "+getTableName()+" where roleId="+roleId,null);
	}

	public List<Long> getRoleIdByUserId(Long userId) {
		List<UserRoleEntity> l=getList("from "+getTableName()+" where userId="+userId,null);
		List<Long> rtnList=new ArrayList<Long>();
		if(l==null){
			return rtnList;
		}
		Map<Long,Long> m=new HashMap<Long,Long>();
		for(UserRoleEntity r:l){
			m.put(r.getRoleId(), 0L);
		}
		for(Long id:m.keySet()){
			rtnList.add(id);
		}
		return rtnList;
	}

	public void clearRoleUser(Long roleId) {
		List<UserRoleEntity> l=getList("from "+getTableName()+" where roleId="+roleId,null);
		String ids="-1,";
		if(l!=null){
			for(UserRoleEntity u:l){
				ids+=u.getId()+",";
			}
		}
		this.batchDel(ids);
	}
}
