package com.ibpd.henuocms.service.permission;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ibpd.dao.impl.BaseServiceImpl;
import com.ibpd.henuocms.entity.PermissionEntity;
@Transactional
@Service("permissionService")
public class PermissionServiceImpl extends BaseServiceImpl<PermissionEntity> implements IPermissionService {
	public PermissionServiceImpl(){
		super();
		this.tableName="PermissionEntity";
		this.currentClass=PermissionEntity.class;
		this.initOK();
	}
}
 