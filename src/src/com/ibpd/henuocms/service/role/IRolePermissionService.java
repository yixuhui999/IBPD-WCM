package com.ibpd.henuocms.service.role;

import java.util.List;

import com.ibpd.dao.impl.IBaseService;
import com.ibpd.henuocms.entity.RolePermissionEntity;

public interface IRolePermissionService extends IBaseService<RolePermissionEntity> {
	List<RolePermissionEntity> getListByRoleId(Long roleId);
	void delRolePermission(Long roleId);
	List<RolePermissionEntity> getListByRoleIds(String ids);
}
 