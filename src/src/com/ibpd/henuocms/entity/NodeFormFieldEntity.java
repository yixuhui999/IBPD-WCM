package com.ibpd.henuocms.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.ibpd.entity.baseEntity.IBaseEntity;

/**
 * 栏目表单字段 
 * @author MG
 * @version 1.0
 */
@Entity
@Table(name="T_NodeFormField")
public class NodeFormFieldEntity  extends IBaseEntity{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id @Column(name="f_id") @GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	/**
	 * 表单ID
	 */
	@Column(name="f_nodeFormId",nullable=true)
	private Long nodeFormId=-1L;
	/**
	 * 字段名称
	 */
	@Column(name="f_fieldName",length=50,nullable=true)
	private String fieldName;
	/**
	 * 显示名称
	 */
	@Column(name="f_displayName",length=255,nullable=true)
	private String displayName;
	/**
	 * 字段分组
	 */
	@Column(name="f_group",length=255,nullable=true)
	private String group;
	/**
	 * 字段说明
	 */
	@Column(name="f_fieldDescription",length=255,nullable=true)
	private String fieldDescription="";
	/**
	 * 是否必填
	 */
	@Column(name="f_isRequired",nullable=true)
	private Boolean isRequired=false;
	/**
	 * 验证组合--未完成
	 */
	@Column(name="f_ExpGroup",length=255,nullable=true)
	private String expGroup="";
	/**
	 * 可选值
	 */
	@Column(name="f_optionalValue",length=255,nullable=true)
	private String optionalValue="";
	/**
	 * 默认值
	 */
	@Column(name="f_defaultValue",length=50,nullable=true)
	private String defaultValue="";
	/**
	 * 控件类型-以后要对找到动态的控件类型表中，控件类型表用来记录控件类型和实现方式
	 * 静态文本框
	 * 动态文本框
	 * 多行文本框
	 * 
	 * 静态下拉框
	 * 动态下拉框
	 * 树形下拉框
	 * 
	 */
	@Column(name="f_TagType",nullable=true)
	private Integer tagType=0;
	/**
	 * 字段是否可编辑
	 */
	@Column(name="f_isEditable",nullable=true)
	private Boolean isEditable=true;
	/**
	 * 添加状态下控件是否可写
	 */
	@Column(name="f_writeByAddMode",nullable=true)
	private Boolean writeByAddMode=false;
	/**
	 * 添加状态下控件是否可见
	 */
	@Column(name="f_displayByAddMode",nullable=true)
	private Boolean displayByAddMode=true;
	/**
	 * 编辑状态下控件是否可写
	 */
	@Column(name="f_writeByEditMode",nullable=true)
	private Boolean writeByEditMode=false;
	/**
	 * 编辑状态下控件是否可见
	 */
	@Column(name="f_displayByEditMode",nullable=true)
	private Boolean displayByEditMode=true;
	/**
	 * 辅助控件--将来链接到辅助控件表中，辅助控件表实现辅助控件的信息和实现
	 * 格式选取、文件选取、用户选取、模板选取
	 */
	@Column(name="f_assistTags",length=255,nullable=true)
	private String assistTags="";
	@Temporal(value=TemporalType.TIMESTAMP)
	@Column(name="f_createTime",nullable=true)
	private Date createTime=new Date();//创建时间
	@Temporal(value=TemporalType.TIMESTAMP)
	@Column(name="f_updateTime",nullable=true)
	private Date updateTime=new Date();//更新时间
	@Column(name="f_order",nullable=true)
	private Integer order=0;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getFieldName() {
		return fieldName;
	}
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	public Boolean getIsRequired() {
		return isRequired;
	}
	public void setIsRequired(Boolean isRequired) {
		this.isRequired = isRequired;
	}

	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public String getFieldDescription() {
		return fieldDescription;
	}
	public void setFieldDescription(String fieldDescription) {
		this.fieldDescription = fieldDescription;
	}
	public String getExpGroup() {
		return expGroup;
	}
	public void setExpGroup(String expGroup) {
		this.expGroup = expGroup;
	}
	public String getOptionalValue() {
		return optionalValue;
	}
	public void setOptionalValue(String optionalValue) {
		this.optionalValue = optionalValue;
	}
	public String getDefaultValue() {
		return defaultValue;
	}
	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}
	public Integer getTagType() {
		return tagType;
	}
	public void setTagType(Integer tagType) {
		this.tagType = tagType;
	}
	public Boolean getWriteByAddMode() {
		return writeByAddMode;
	}
	public void setWriteByAddMode(Boolean writeByAddMode) {
		this.writeByAddMode = writeByAddMode;
	}
	public Boolean getDisplayByAddMode() {
		return displayByAddMode;
	}
	public void setDisplayByAddMode(Boolean displayByAddMode) {
		this.displayByAddMode = displayByAddMode;
	}
	public Boolean getWriteByEditMode() {
		return writeByEditMode;
	}
	public void setWriteByEditMode(Boolean writeByEditMode) {
		this.writeByEditMode = writeByEditMode;
	}
	public Boolean getDisplayByEditMode() {
		return displayByEditMode;
	}
	public void setDisplayByEditMode(Boolean displayByEditMode) {
		this.displayByEditMode = displayByEditMode;
	}
	public String getAssistTags() {
		return assistTags;
	}
	public void setAssistTags(String assistTags) {
		this.assistTags = assistTags;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	public Integer getOrder() {
		return order;
	}
	public void setOrder(Integer order) {
		this.order = order;
	}
	public void setNodeFormId(Long nodeFormId) {
		this.nodeFormId = nodeFormId;
	}
	public Long getNodeFormId() {
		return nodeFormId;
	}
	public void setIsEditable(Boolean isEditable) {
		this.isEditable = isEditable;
	}
	public Boolean getIsEditable() {
		return isEditable;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	public String getGroup() {
		return group;
	}
	public enum TagType {
		/**
		 * 控件类型-以后要对找到动态的控件类型表中，控件类型表用来记录控件类型和实现方式
		 * 静态文本框
		 * 动态文本框
		 * 多行文本框
		 * 
		 * 静态下拉框
		 * 动态下拉框
		 * 树形下拉框
		 * 
		 */
		STATIC_TEXT(0),
		DYNAMIC_TEXT(1),
		MULTILINE_TEXT(2),
		STATIC_COMBO(3),
		DYNAMIC_COMBO(4),
		TREE_COMBO(5);
        private Integer nCode;
        private TagType(Integer _nCode) {

            this.nCode = _nCode;

        }

        public Integer getValue(){
        	return nCode;
        }
        @Override
        public String toString() {

            return String.valueOf(this.nCode);

        }

    }

	public enum AssistType{
		FileSelecter("fileSelecter"),
		StyleTemplateSelecter("styleTemplateSelecter"),
		FormSelecter("formSelecter"),
		UserSelecter("userSelecter"),
		PageTemplateSelecter("pageTemplateSelecter");
		String _type="";
		private AssistType(String type){
			this._type=type;
		}
        @Override
        public String toString() {

            return _type;

        }
	}
}
