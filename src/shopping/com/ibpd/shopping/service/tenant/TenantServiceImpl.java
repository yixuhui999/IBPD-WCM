package com.ibpd.shopping.service.tenant;

import java.util.List;

import org.springframework.stereotype.Service;

import com.ibpd.dao.impl.BaseServiceImpl;
import com.ibpd.shopping.entity.TenantEntity;
@Service("tenantService") 
public class TenantServiceImpl extends BaseServiceImpl<TenantEntity> implements ITenantService {
	public TenantServiceImpl(){
		super();
		this.tableName="TenantEntity";
		this.currentClass=TenantEntity.class;
		this.initOK();
	}

	public List<TenantEntity> getListByIds(String[] ids) {
		if(ids!=null && ids.length>0){
			String hql="from "+getTableName()+" where ";
			for(String id:ids){
				hql+="id="+id+" or ";
			}
			hql=hql.substring(0,hql.length()-4);
			return getList(hql,null);
		}
		return null;
	}
}
