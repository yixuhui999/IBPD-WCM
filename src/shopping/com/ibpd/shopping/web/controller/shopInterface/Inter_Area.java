package com.ibpd.shopping.web.controller.shopInterface;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ibpd.henuocms.web.controller.manage.BaseController;
import com.ibpd.shopping.entity.AreaEntity;
import com.ibpd.shopping.service.area.AreaServiceImpl;
import com.ibpd.shopping.service.area.IAreaService;
@Controller
public class Inter_Area  extends BaseController{
	@RequestMapping("getAreaByParentId.do")
	public void getAreaByParentId(org.springframework.ui.Model model,HttpServletResponse resp,HttpServletRequest req,String pid){
		IAreaService areaServ=(IAreaService) getService(AreaServiceImpl.class);
		List<AreaEntity> areList=areaServ.getAreaListByParentCode(pid);
		super.sendJsonByList(resp, areList, Long.valueOf(areList.size()));
	}
}
