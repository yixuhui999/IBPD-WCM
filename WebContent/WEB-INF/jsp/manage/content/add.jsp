<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>${pageTitle }</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>css/easyui.css" />
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>js/themes/default/easyui.css" />
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>js/themes/icon.css" />
		<link type="text/css" href="<%=basePath %>css/forms.css" />
		<style type="text/css">
		.baseInfo{
			width:210px;
			
		}
		.baseInfo tr .tit{
			font-size:10px;
			
			overflow:hidden;
			width:80px;
		}
		.baseInfo tr td{
			padding:2 2 2 2;
			width:100px;
			border-bottom:solid 1px #464646;
		}
		.baseInfo tr td input {
			width:150px;
		}
		.baseInfo tr td select {
			width:150px;
		}
		.baseInfo tr td textarea {
			width:150px;
		}
		/*
		.hidden20150323{
			display:none;
		}
		*/
		</style> 
			
    <script type="text/javascript" src="<%=basePath %>js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="<%=basePath %>js/Validform_v5.3.2_ncr_min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/easyui-lang-zh_CN.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/ckeditor/ckeditor.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/custom.js"></script>
	
	<script type="text/javascript">
		var basePath='<%=path %>';
	</script>
	</head>

	<body>
	<div class="easyui-layout" fit="true">
   
    <div data-options="region:'center'"  id="center" region="center" style="background: #eee; overflow-y:hidden">
   	 <textarea id="text">${text }</textarea>
    </div>
   <div data-options="region:'east'" region="east" split="false" title="属性配置" style="width:300px;" id="west">
						<c:if test="${type=='add' }">
							<input mthod="smt" name="nodeId" id="s_nodeId" type="hidden" value="${nodeId }"/>
						</c:if>
						<c:if test="${type=='edit' }">
							<input mthod="smt" name="nodeId" id="s_nodeId" type="hidden" value="${entity.nodeId }"/>
							<input mthod="smt" name="id" id="s_id" type="hidden" value="${entity.id }"/>
						</c:if>
		<table class="baseInfo" cellpadding="0" cellspacing="0">
			
			<!--tr> 
				<td class="tit">title</td>
				<td class="val">
				<span>
					<span style="width:120px;">
						<input type="text" mthod="smt" style="width:120px;" class="easyui-textbox" id="s_title" name="title" value="${entity.title }"/>
						<input name="titleFormatParams" id="s_titleFormatParams" type="hidden" value="${titleFormatParameters }"/>
					</span>
					<span style="width:20px;">
						<input type="button" style="width:20px;" value="..." id="title_btn">
					</span>
				</span>
					
				</td>
			</tr-->
				<c:forEach items="${fields }" var="field">
				<c:if test="${field.displayByAddMode==true }">
					<tr <c:if test="${field.displayByAddMode==false }">style="display:none;"</c:if>>
						<td class="tit" <c:if test="${field.displayByAddMode==false }">style="display:none;"</c:if>><div class="tittext">${field.displayName }</div></td>
						<td class="val">
							
							<c:if test="${field.tagType=='0' && field.displayByAddMode==true }">
							<span class="int"><input type="text" mthod="smt" id="s_${field.fieldName }" name="${field.fieldName }" value="<c:if test="${type=='edit' }">${entity[field.fieldName] }</c:if><c:if test="${type=='add' }">${field.defaultValue }</c:if>" <c:if test="${field.writeByAddMode==false }">readonly disabled</c:if> <c:if test="${field.displayByAddMode==false }">style="display:none;"</c:if>/></span>
							</c:if>
							<c:if test="${field.tagType=='1' && field.displayByAddMode==true }">
							<span class="int"><input type="text" mthod="smt" id="s_${field.fieldName }" name="${field.fieldName }" value="<c:if test="${type=='edit' }">${entity[field.fieldName] }</c:if><c:if test="${type=='add' }">${field.defaultValue }</c:if>" <c:if test="${field.writeByAddMode==false }">readonly disabled</c:if> <c:if test="${field.displayByAddMode==false }">style="display:none;"</c:if>/></span>
							</c:if>
							<c:if test="${field.tagType=='2' && field.displayByAddMode==true }">
							<span class="int"><textarea mthod="smt" id="s_${field.fieldName }" name="${field.fieldName }" <c:if test="${field.writeByAddMode==false }">readonly disabled</c:if> <c:if test="${field.displayByAddMode==false }">style="display:none;"</c:if>><c:if test="${type=='edit' }">${entity[field.fieldName] }</c:if><c:if test="${type=='add' }">${field.defaultValue }</c:if></textarea></span>
							</c:if>
							<c:if test="${field.tagType=='3' && field.displayByAddMode==true }">
							<span class="int">
							<select mthod="smt" id="s_${field.fieldName }" name="${field.fieldName }" <c:if test="${field.writeByAddMode==false }">readonly disabled</c:if> <c:if test="${field.displayByAddMode==false }">style="display:none;"</c:if>>
							<c:set value="${ fn:split(field.optionalValue, ';') }" var="values" />
							<c:if test="${type=='edit' }"><c:set var="val" value="${entity[field.fieldName] }"></c:set></c:if><c:if test="${type=='add' }"><c:set var="val" value="${field.defaultValue }"></c:set></c:if>
							
								<c:forEach items="${values }" var="value">
									<c:set value="${ fn:split(value, ':') }" var="v" />
										<option value="${v[0] }" <c:if test="${v[0]==val }">selected</c:if>>${v[1] }</option>
								</c:forEach>

							</select>
							</span>
							</c:if>
							<c:if test="${field.tagType=='4' && field.displayByAddMode==true }">
							<span class="int">
							<select mthod="smt" id="s_${field.fieldName }" name="${field.fieldName }" <c:if test="${field.writeByAddMode==false }">readonly disabled</c:if> <c:if test="${field.displayByAddMode==false }">style="display:none;"</c:if>>
							<c:set value="${ fn:split(field.optionalValue, ';') }" var="values" />
							<c:if test="${type=='edit' }"><c:set var="val" value="${entity[field.fieldName] }"></c:set></c:if><c:if test="${type=='add' }"><c:set var="val" value="${field.defaultValue }"></c:set></c:if>
								<c:forEach items="${values }" var="value">
									<c:set value="${ fn:split(value, ':') }" var="v" />
										<option value="${v[0] }" <c:if test="${v[0]==val }">selected</c:if>>${v[1] }</option>
								</c:forEach>
							</select>
							</span>
							</c:if>
							<c:if test="${field.tagType=='5' && field.displayByAddMode==true }">
							<span class="int">
							<select mthod="smt" id="s_${field.fieldName }" name="${field.fieldName }" <c:if test="${field.writeByAddMode==false }">readonly disabled</c:if> <c:if test="${field.displayByAddMode==false }">style="display:none;"</c:if>>
							<c:set value="${ fn:split(field.optionalValue, ';') }" var="values" />
							<c:if test="${type=='edit' }"><c:set var="val" value="${entity[field.fieldName] }"></c:set></c:if><c:if test="${type=='add' }"><c:set var="val" value="${field.defaultValue }"></c:set></c:if>
								<c:forEach items="${values }" var="value">
									<c:set value="${ fn:split(value, ':') }" var="v" />
										<option value="${v[0] }" <c:if test="${v[0]==val }">selected</c:if>>${v[1] }</option>
								</c:forEach>
							</select>
							</span>
							</c:if>
							</span>
							<!--辅助控件部分-->
							<c:if test="${field.assistTags!='none' && field.assistTags!='' && field.displayByAddMode==true  }"><span class="assist"></c:if>
							<c:if test="${field.assistTags=='formatter' }">
								<input type="button" smt="formatter" name="${field.fieldName }_formatter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='fileSelecter' }">
								<input type="checkbox" name="${field.fieldName }_fileAppend" style="width:20px"/>追加
								<input type="button" smt="fileSelecter" name="${field.fieldName }_fileSelecter" value="..." style="width:80px"/>
							</c:if>
							<c:if test="${field.assistTags=='userSelecter' }">
								<input type="button" smt="userSelecter" name="${field.fieldName }_userSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='templateSelecter' }">
								<input type="button" smt="templateSelecter" name="${field.fieldName }_templateSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='dateSelecter' }">
								<input type="button" smt="dateSelecter" name="${field.fieldName }_dateSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='timeSelecter' }">
								<input type="button" smt="timeSelecter" name="${field.fieldName }_timeSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='dateTimeSelecter' }">
								<input type="button" smt="dateTimeSelecter" name="${field.fieldName }_dateTimeSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='colorSelecter' }">
								<input type="button" smt="colorSelecter" name="${field.fieldName }_colorSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags!='none' && field.assistTags!='' && field.displayByAddMode==true  }"></span></c:if>
						</td>
						
					</tr>
					</c:if>
				</c:forEach>
			<tr class="hidden20150323">
				<td class="tit">复制到</td>
				<td class="val">
					<input class="easyui-combotree" id="copyTo" value="${copyToNode}">
				</td>
			</tr>
			<tr class="hidden20150323">
				<td class="tit">分发到</td> 
				<td class="val">  
					
					<input class="easyui-combotree" id="linkTo" value="${linkToNode }">
				</td>
			</tr>
		</table>
	    
    </div>
    <div id="top" data-options="region:'north'" region="north" style="background: #eee; overflow-y:hidden">
    <div class="datagrid-toolbar">
    	<table cellspacing="0" cellpadding="0">
    		<tbody>
    			<tr>
    				<td>
    					<a href="javascript:void(0)" class="l-btn l-btn-small l-btn-plain" group="" id="save">
    					<span class="l-btn-left l-btn-icon-left"><span class="l-btn-text">保存</span>
    						<span class="l-btn-icon icon-add">&nbsp;</span>
    					</span>
    					</a>
    				</td>
     				<td>
    					
    				<br></td>
    				<td>
    					<a href="javascript:void(0)" class="l-btn l-btn-small l-btn-plain" group="" id="close"><span class="l-btn-left l-btn-icon-left"><span class="l-btn-text">关闭</span><span class="l-btn-icon icon-remove">&nbsp;</span></span></a>
    				</td>
    			</tr>
    		</tbody>
    	</table>
    </div>
    </div>
	</div>

	<script type="text/javascript">
	var path="<%=path %>";
	var textEditor=null;
	var process=null;
	var type="${type }";
	var nodeId="${nodeId }";
	var contentId="";
	if(type=='edit'){
		nodeId="${entity.nodeId }";
		contentId="${entity.id}";
	}
	var copyToId=getsp("${copyTo }");
	var linkToId=getsp("${linkTo }");
	function getsp(s){
	return s.split(":");
	};
	function bindEvents(){
		$("input[smt='formatter']").click(function(e){parent.showFormatterDialog(e);});
		$("input[smt='fileSelecter']").click(function(e){selectFile(e);});
		$("input[smt='userSelecter']").click(function(e){parent.showUserSelecterDialog(e);});
		$("input[smt='templateSelecter']").click(function(e){parent.showTemplateSelecter(e);});
		$("input[smt='dateSelecter']").click(function(e){});//不需要上级页面帮忙完成显示
		$("input[smt='timeSelecter']").click(function(e){});//不需要上级页面帮忙完成显示
		$("input[smt='dateTimeSelecter']").click(function(e){});//不需要上级页面帮忙完成显示
		$("input[smt='colorSelecter']").click(function(e){});//不需要上级页面帮忙完成显示
	};	
	//由于本页面是嵌套在IFRAME中的，所以在展现一些对话框时，无法显示到本页（被挡住了，放不下），所以需要 调用上级页面的对话框显示函数，然后函数再调用下面的这个方法设置值
	/**
	*
	**/
	function setValue(name,val){
		$("[name='"+name+"']").val(val);
		$("[name='"+name+"']").blur();
	};

	$(document).ready(function(){
		textEditor=CKEDITOR.replace('text');
		
		//textEditor.on("instanceReady",function(){textEditor.setData("${text }");});
	
		setTimeout(function(){$("#cke_1_contents").height($("#cke_1_contents").height()+$("#center").height()-$("#cke_text").height());},500);
		$("#title_btn").bind("click",{s:this},formatTitle);
		$("#save").click(function(){save();});
		$("#close").click(function(){closeCurWindow();});
		$("input[itype='fileSelect']").click(function(e){selectFile(e);});
		$("#copyTo").combotree({url:path+'/Manage/Node/tree.do',method:'post',multiple:true,onlyLeafCheck:true,onLoadSuccess:function(){$("#copyTo").combotree("setValues",copyToId)}});
		$("#linkTo").combotree({url:path+'/Manage/Node/tree.do',method:'post',multiple:true,onlyLeafCheck:true,onLoadSuccess:function(){$("#linkTo").combotree("setValues",linkToId)}});
		if(type=='edit'){
			$("#copyTo").combotree({disabled:true});
			$("#linkTo").combotree({disabled:true});
		}
		bindEvents();
	}); 
	function selectFile(e){
		curImageId=$(e.target).attr("name").split("_")[0];
		window.open(basePath+'/Manage/FileSelecter/index.do?nodeId='+nodeId+'&filter='+curImageId,'文件上传','toolbar=no,menubar=no,resizable=no,location=no,status=no,z-look:yes');
	};
	function changeFile(s,f){
		if(s!=""){
			s=s.substring(0,s.length-1);
		}
		if($("input[name='"+curImageId+"_fileAppend']:checked").length>0){
			$("input[name='"+f+"']").val($("input[name='"+f+"']").val()+";"+s);
		}else{
			$("input[name='"+f+"']").val(s);
		}
       
	};

	function selectFile2(e){
		var filter=$(e.target).attr("iname");
		var ifm=$("<div id='selectFileDialog'/>").appendTo('body');
		$(ifm).dialog({
        	modal:true,
        	title:'选择文件',
        	shadow:true,
        	iconCls:'icon-edit',
        	width:820,
        	height:620,
        	resizable:true,
        	toolbar:[{
                    text:'关闭窗口',
                    iconCls:'icon-save',
                    handler:function(){
                    	$(ifm).dialog("close");
						$(ifm).remove();
                    }
                }], 
        	content:"<iframe id=\"ifm\" src='"+path+"/Manage/FileSelecter/index.do?nodeId="+nodeId+"&filter="+filter+"' frameborder='0' width='800px' height='550px'></iframe>"
        }); 
        $(ifm).dialog("open");
	};
	function changeFile2(s,f){
	if(s!=""){
		s=s.substring(0,s.length-1);
	}
	$("input[name='"+f+"']").val(s);
	$("#selectFileDialog").dialog("close");
	$("#selectFileDialog").remove();
	};
	function save(){
	
		var ss=$("[mthod='smt']"); 
		//var dd=$("select[mthod]");
		var p="";  
		for(s=0;s<ss.length;s++){
			p+=""+ss[s].name+":'"+$(ss[s]).val()+"',";
		} 
		//for(s=0;s<dd.length;s++){
		//	p+=""+dd[s].name+":'"+$(dd[s]).val()+"',";
		//} 
		if(p.length>0){
			//p="{"+p+"'titleFormat':'"+$("#s_titleFormatParams").val()+"'}");
			p="{"+p+"titleFormatParams:'"+$("#s_titleFormatParams").val()+"',";
			p=""+p+"copyTo:'"+$("#copyTo").combotree("getValues")+"',";
			p=""+p+"linkTo:'"+$("#linkTo").combotree("getValues")+"'}";
			process = $.messager.progress({
                title:'稍后',
                msg:'正在保存基本信息,请稍候...'
            });
            var savePath=path+"/Manage/Content/doAdd.do";
            if(type=="edit"){
            	savePath=path+"/Manage/Content/doEdit.do"
            }
			$.post(
				savePath,
				eval("["+p+"]")[0],
				function(result){
					$.messager.progress('close');
					var res=eval("["+result+"]");
					if(res[0].status=='99'){
						process = $.messager.progress({
	                		title:'稍后',
	                		msg:'正在保存正文,请稍候...'
	           			});
						saveContent(res[0].id);
					}else{
						msgShow("提示",res[0].msg+"<br/>错误代码:"+res[0].status,"error");
					}
				}
			); 
		}
	};
	function saveContent(id){
			$.post(
				path+"/Manage/Content/saveText.do",
				{id:id,content:textEditor.getData()},
				function(result){
					$.messager.progress('close');
					var res=eval("["+result+"]");
					if(res[0].status=='99'){
						window.opener.reload();
						 $.messager.confirm("提示","保存成功,是否继续新建文档?",function(r){if(r){newDoc();}else{closeCurWindow();}});
					}else{
						msgShow("提示",res[0].msg+"<br/>错误代码:"+res[0].status,"error");
					}
				}
			); 
	};
	function newDoc(){ 
		var ss=$("[mthod]"); 
		for(i=0;i<ss.length;i++){
			if($(ss[i]).attr("name")!="nodeId")
				$(ss[i]).val("");
		}
		textEditor.setData('');
	}; 
	function formatTitle(s){
        var formatDialog = $('<div id="formatTitleDlg"/>').appendTo('body');
      
        $(formatDialog).dialog({
        	modal:true,
        	title:'格式化标题',
        	shadow:true,
        	iconCls:'icon-edit',
        	width:500,
        	height:300,
        	resizable:true,
        	toolbar:[{
                    text:'保存',
                    iconCls:'icon-save',
                    handler:function(){
                    	var fval=$("#formatframe")[0].contentWindow.submit();
                        if(fval!=''){
	                        $(formatDialog).dialog("close");
	                        $(formatDialog).remove();
	                        $("#s_titleFormatParams").val("color:"+fval.curColor+";strong:" + fval.strong+";em:"+fval.em+";u:"+fval.u+";size:"+fval.size);
                        }else{
                        	
                        }
                    }
                },'-',{
                    text:'取消',
                    iconCls:'icon-cancel',
                    handler:function(){ 
                        $(formatDialog).dialog("close");
	                    $(formatDialog).remove();
                    }
                }], 
        	content:'<iframe id="formatframe" width="480px" height="230px" scrolling="no" frameborder="no" style="overflow:hidden;" src="<%=basePath %>/Manage/Content/formatTitle.do?txt='+$("#s_title").val()+'"></iframe>'
        }); 
        $(formatDialog).dialog("open");
	};
	//弹出信息窗口 title:标题 msgString:提示信息 msgType:信息类型 [error,info,question,warning]
	function msgShow(title, msgString, msgType) {
		$.messager.alert(title, msgString, msgType);
	};
     </script>
	</body>
</html>