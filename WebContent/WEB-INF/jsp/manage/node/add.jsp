<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>栏目管理</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>css/easyui.css" />
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>js/themes/icon.css" />
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>js/poshytip-1.2/src/tip-yellow/tip-yellow.css" />
		
			
		<style type="text/css">
		.easyui-tabs table{
			width:500px;
			margin:10 10 10 10;
		}
		.easyui-tabs table tr .tit{
			width:30%;
		} 
		.easyui-tabs table tr .val{
			width:60%;
		} 
		.easyui-tabs table tr .val input{
			margin:2 3 2 3;
			width:96%;
		} 
		.easyui-tabs table tr .val select{
			margin:2 3 2 3;
			width:96%;
		} 
		/*
		.hidden20150323{
			display:none;
		}*/
		</style>
    <script type="text/javascript" src="<%=basePath %>js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="<%=basePath %>js/Validform_v5.3.2_ncr_min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/jquery.validatebox.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/extValidatebox.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/poshytip-1.2/src/jquery.poshytip.min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/easyui-lang-zh_CN.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/datese/lhgcore.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/datese/lhgcalendar.js"></script>
	</head>

	<body style="margin:0;padding:0;text-align:center;"> 
	
	<div class="easyui-layout" fit="true">
   <div region="west" hide="true" split="false" fit="true" id="west">
	<div class="easyui-tabs"  fit="true" border="false" id="fm">
	<div title="基本信息">
    	<input type="hidden" id="entityId" name="nodeEntity.id" value="${entity.id }"/>
    	<input type="hidden" mthod="smt" name="nodeEntity.parentId" id="parentId" value="${parentId }"/>
    	<input type="hidden" mthod="smt" name="nodeEntity.subSiteId" id="subSiteId" value="${siteId }"/>
    	<table cellpadding="0" cellspacing="0" border="0">
				<c:forEach items="${base }" var="field">
				<c:if test="${field.displayByAddMode==true }">
					<tr <c:if test="${field.displayByAddMode==false }">style="display:none;"</c:if>>
						<td class="tit" <c:if test="${field.displayByAddMode==false }">style="display:none;"</c:if>><div class="tittext">${field.displayName }</div></td>
						<td class="val">
							
							<c:if test="${field.tagType=='0' && field.displayByAddMode==true }">
							<span class="int"><input type="text" mthod="smt" name="nodeEntity.${field.fieldName }" value="${field.defaultValue }" <c:if test="${field.writeByAddMode==false }">readonly disabled</c:if> <c:if test="${field.displayByAddMode==false }">style="display:none;"</c:if> <c:if test="${field.displayByAddMode==true && field.writeByAddMode==true && field.isRequired==true && (field.expGroup=='' || field.expGroup==null) }"> class="easyui-validatebox" data-options="required:true"</c:if>
<c:if test="${field.displayByAddMode==true && field.writeByAddMode==true && field.isRequired==true && field.expGroup!='' }"> class="easyui-validatebox" data-options="required:true,validType:'${field.expGroup}'"</c:if>
<c:if test="${field.displayByAddMode==true && field.writeByAddMode==true && field.isRequired==false && field.expGroup!='' }"> class="easyui-validatebox" data-options="validType:'${field.expGroup}'"</c:if>/></span>
							</c:if>
							<c:if test="${field.tagType=='1' && field.displayByAddMode==true }">
							<span class="int"><input type="text" mthod="smt" name="nodeEntity.${field.fieldName }" value="${field.defaultValue }" <c:if test="${field.writeByAddMode==false }">readonly disabled</c:if> <c:if test="${field.displayByAddMode==false }">style="display:none;"</c:if><c:if test="${field.displayByAddMode==true && field.writeByAddMode==true && field.isRequired==true && (field.expGroup=='' || field.expGroup==null) }"> class="easyui-validatebox" data-options="required:true"</c:if>
<c:if test="${field.displayByAddMode==true && field.writeByAddMode==true && field.isRequired==true && field.expGroup!='' }"> class="easyui-validatebox" data-options="required:true,validType:'${field.expGroup}'"</c:if>
<c:if test="${field.displayByAddMode==true && field.writeByAddMode==true && field.isRequired==false && field.expGroup!='' }"> class="easyui-validatebox" data-options="validType:'${field.expGroup}'"</c:if>/>/></span>
							</c:if>
							<c:if test="${field.tagType=='2' && field.displayByAddMode==true }">
							<span class="int"><textarea mthod="smt" name="nodeEntity.${field.fieldName }" <c:if test="${field.writeByAddMode==false }">readonly disabled</c:if> <c:if test="${field.displayByAddMode==false }">style="display:none;"</c:if><c:if test="${field.displayByAddMode==true && field.writeByAddMode==true && field.isRequired==true && (field.expGroup=='' || field.expGroup==null) }"> class="easyui-validatebox" data-options="required:true"</c:if>
<c:if test="${field.displayByAddMode==true && field.writeByAddMode==true && field.isRequired==true && field.expGroup!='' }"> class="easyui-validatebox" data-options="required:true,validType:'${field.expGroup}'"</c:if>
<c:if test="${field.displayByAddMode==true && field.writeByAddMode==true && field.isRequired==false && field.expGroup!='' }"> class="easyui-validatebox" data-options="validType:'${field.expGroup}'"</c:if>/>>${field.defaultValue }</textarea></span>
							</c:if>
							<c:if test="${field.tagType=='3' && field.displayByAddMode==true }">
							<span class="int">
							<select mthod="smt" name="nodeEntity.${field.fieldName }" <c:if test="${field.writeByAddMode==false }">readonly disabled</c:if> <c:if test="${field.displayByAddMode==false }">style="display:none;"</c:if>>
							<c:set value="${ fn:split(field.optionalValue, ';') }" var="values" />
								<c:forEach items="${values }" var="value">
									<c:set value="${ fn:split(value, ':') }" var="v" />
										<option value="${v[0] }" <c:if test="${v[0]==field.defaultValue }">selected</c:if>>${v[1] }</option>
								</c:forEach>
							</select>
							</span>
							</c:if>
							<c:if test="${field.tagType=='4' && field.displayByAddMode==true }">
							<span class="int">
							<select mthod="smt" name="nodeEntity.${field.fieldName }" <c:if test="${field.writeByAddMode==false }">readonly disabled</c:if> <c:if test="${field.displayByAddMode==false }">style="display:none;"</c:if>>
							<c:set value="${ fn:split(field.optionalValue, ';') }" var="values" />
								<c:forEach items="${values }" var="value">
									<c:set value="${ fn:split(value, ':') }" var="v" />
										<option value="${v[0] }" <c:if test="${v[0]==field.defaultValue }">selected</c:if>>${v[1] }</option>
								</c:forEach>
							</select>
							</span>
							</c:if>
							<c:if test="${field.tagType=='5' && field.displayByAddMode==true }">
							<span class="int">
							<select mthod="smt" name="nodeEntity.${field.fieldName }" <c:if test="${field.writeByAddMode==false }">readonly disabled</c:if> <c:if test="${field.displayByAddMode==false }">style="display:none;"</c:if>>
							<c:set value="${ fn:split(field.optionalValue, ';') }" var="values" />
								<c:forEach items="${values }" var="value">
									<c:set value="${ fn:split(value, ':') }" var="v" />
										<option value="${v[0] }" <c:if test="${v[0]==field.defaultValue }">selected</c:if>>${v[1] }</option>
								</c:forEach>
							</select>
							</span>
							</c:if>
							</span>
							<!--辅助控件部分-->
							<c:if test="${field.assistTags!='none' && field.assistTags!='' && field.displayByAddMode==true  }"><span class="assist"></c:if>
							<c:if test="${field.assistTags=='formatter' }">
								<input type="button" smt="formatter" name="nodeEntity.${field.fieldName }_formatter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='fileSelecter' }">
								<input type="button" smt="fileSelecter" name="nodeEntity.${field.fieldName }_fileSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='userSelecter' }">
								<input type="button" smt="userSelecter" name="nodeEntity.${field.fieldName }_userSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='templateSelecter' }">
								<input type="button" smt="templateSelecter" name="nodeEntity.${field.fieldName }_templateSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='dateSelecter' }">
								<input type="button" smt="dateSelecter" name="nodeEntity.${field.fieldName }_dateSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='timeSelecter' }">
								<input type="button" smt="timeSelecter" name="nodeEntity.${field.fieldName }_timeSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='dateTimeSelecter' }">
								<input type="button" smt="dateTimeSelecter" name="nodeEntity.${field.fieldName }_dateTimeSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='colorSelecter' }">
								<input type="button" smt="colorSelecter" name="nodeEntity.${field.fieldName }_colorSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags!='none' && field.assistTags!='' && field.displayByAddMode==true  }"></span></c:if>
						</td>
						
					</tr>
					</c:if>
				</c:forEach>
					<tr class="hidden20150323">
						<td class="tit"><div class="tittext">导航中显示</div></th>
						<td class="val">
							<span class="int">
							<select mthod="smt" name="attrEntity.displayInNavigator">
								<option value="true" <c:if test="${attrEntity.displayInNavigator==true }">selected</c:if>>显示</option>
								<option value="false" <c:if test="${attrEntity.displayInNavigator==false }">selected</c:if>>隐藏</option>
							</select>
							</span>
						</td>
					</tr>
					<tr class="hidden20150323">
						<td class="tit"><div class="tittext">导航排序</div></th>
						<td class="val">
							<span class="int">
							<input type="text" mthod="smt" name="attrEntity.orderByNavigator" value="${attrEntity.orderByNavigator }"/>
							</span>
						</td>
					</tr>
					<tr class="hidden20150323">
						<td class="tit"><div class="tittext">存放目录</div></th>
						<td class="val">
							<span class="int">
							<input type="text" mthod="smt" name="attrEntity.directory" value="${attrEntity.directory }"/>
							</span>
						</td>
					</tr>
		</table>
    	
    </div>
    <div title="展现设置">
 	  	<table cellpadding="0" cellspacing="0" border="0">
				<c:forEach items="${view }" var="field">
				<c:if test="${field.displayByAddMode==true }">
					<tr <c:if test="${field.displayByAddMode==false }">style="display:none;"</c:if>>
						<td class="tit" <c:if test="${field.displayByAddMode==false }">style="display:none;"</c:if>><div class="tittext">${field.displayName }</div></td>
						<td class="val">
							
							<c:if test="${field.tagType=='0' && field.displayByAddMode==true }">
							<span class="int"><input type="text" mthod="smt" name="nodeEntity.${field.fieldName }" value="${field.defaultValue }" <c:if test="${field.writeByAddMode==false }">readonly disabled</c:if> <c:if test="${field.displayByAddMode==false }">style="display:none;"</c:if><c:if test="${field.displayByAddMode==true && field.writeByAddMode==true && field.isRequired==true && (field.expGroup=='' || field.expGroup==null) }"> class="easyui-validatebox" data-options="required:true"</c:if>
<c:if test="${field.displayByAddMode==true && field.writeByAddMode==true && field.isRequired==true && field.expGroup!='' }"> class="easyui-validatebox" data-options="required:true,validType:'${field.expGroup}'"</c:if>
<c:if test="${field.displayByAddMode==true && field.writeByAddMode==true && field.isRequired==false && field.expGroup!='' }"> class="easyui-validatebox" data-options="validType:'${field.expGroup}'"</c:if>/></span>
							</c:if>
							<c:if test="${field.tagType=='1' && field.displayByAddMode==true }">
							<span class="int"><input type="text" mthod="smt" name="nodeEntity.${field.fieldName }" value="${field.defaultValue }" <c:if test="${field.writeByAddMode==false }">readonly disabled</c:if> <c:if test="${field.displayByAddMode==false }">style="display:none;"</c:if><c:if test="${field.displayByAddMode==true && field.writeByAddMode==true && field.isRequired==true && (field.expGroup=='' || field.expGroup==null) }"> class="easyui-validatebox" data-options="required:true"</c:if>
<c:if test="${field.displayByAddMode==true && field.writeByAddMode==true && field.isRequired==true && field.expGroup!='' }"> class="easyui-validatebox" data-options="required:true,validType:'${field.expGroup}'"</c:if>
<c:if test="${field.displayByAddMode==true && field.writeByAddMode==true && field.isRequired==false && field.expGroup!='' }"> class="easyui-validatebox" data-options="validType:'${field.expGroup}'"</c:if>/></span>
							</c:if>
							<c:if test="${field.tagType=='2' && field.displayByAddMode==true }">
							<span class="int"><textarea mthod="smt" name="nodeEntity.${field.fieldName }" <c:if test="${field.writeByAddMode==false }">readonly disabled</c:if> <c:if test="${field.displayByAddMode==false }">style="display:none;"</c:if><c:if test="${field.displayByAddMode==true && field.writeByAddMode==true && field.isRequired==true && (field.expGroup=='' || field.expGroup==null) }"> class="easyui-validatebox" data-options="required:true"</c:if>
<c:if test="${field.displayByAddMode==true && field.writeByAddMode==true && field.isRequired==true && field.expGroup!='' }"> class="easyui-validatebox" data-options="required:true,validType:'${field.expGroup}'"</c:if>
<c:if test="${field.displayByAddMode==true && field.writeByAddMode==true && field.isRequired==false && field.expGroup!='' }"> class="easyui-validatebox" data-options="validType:'${field.expGroup}'"</c:if>>${field.defaultValue }</textarea></span>
							</c:if>
							<c:if test="${field.tagType=='3' && field.displayByAddMode==true }">
							<span class="int">
							<select mthod="smt" name="nodeEntity.${field.fieldName }" <c:if test="${field.writeByAddMode==false }">readonly disabled</c:if> <c:if test="${field.displayByAddMode==false }">style="display:none;"</c:if>>
							<c:set value="${ fn:split(field.optionalValue, ';') }" var="values" />
								<c:forEach items="${values }" var="value">
									<c:set value="${ fn:split(value, ':') }" var="v" />
										<option value="${v[0] }" <c:if test="${v[0]==field.defaultValue }">selected</c:if>>${v[1] }</option>
								</c:forEach>
							</select>
							</span>
							</c:if>
							<c:if test="${field.tagType=='4' && field.displayByAddMode==true }">
							<span class="int">
							<select mthod="smt" name="nodeEntity.${field.fieldName }" <c:if test="${field.writeByAddMode==false }">readonly disabled</c:if> <c:if test="${field.displayByAddMode==false }">style="display:none;"</c:if>>
							<c:set value="${ fn:split(field.optionalValue, ';') }" var="values" />
								<c:forEach items="${values }" var="value">
									<c:set value="${ fn:split(value, ':') }" var="v" />
										<option value="${v[0] }" <c:if test="${v[0]==field.defaultValue }">selected</c:if>>${v[1] }</option>
								</c:forEach>
							</select>
							</span>
							</c:if>
							<c:if test="${field.tagType=='5' && field.displayByAddMode==true }">
							<span class="int">
							<select mthod="smt" name="nodeEntity.${field.fieldName }" <c:if test="${field.writeByAddMode==false }">readonly disabled</c:if> <c:if test="${field.displayByAddMode==false }">style="display:none;"</c:if>>
							<c:set value="${ fn:split(field.optionalValue, ';') }" var="values" />
								<c:forEach items="${values }" var="value">
									<c:set value="${ fn:split(value, ':') }" var="v" />
										<option value="${v[0] }" <c:if test="${v[0]==field.defaultValue }">selected</c:if>>${v[1] }</option>
								</c:forEach>
							</select>
							</span>
							</c:if>
							</span>
							<!--辅助控件部分-->
							<c:if test="${field.assistTags!='none' && field.assistTags!='' && field.displayByAddMode==true  }"><span class="assist"></c:if>
							<c:if test="${field.assistTags=='formatter' }">
								<input type="button" smt="formatter" name="nodeEntity.${field.fieldName }_formatter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='fileSelecter' }">
								<input type="button" smt="fileSelecter" name="nodeEntity.${field.fieldName }_fileSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='userSelecter' }">
								<input type="button" smt="userSelecter" name="nodeEntity.${field.fieldName }_userSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='templateSelecter' }">
								<input type="button" smt="templateSelecter" name="nodeEntity.${field.fieldName }_templateSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='dateSelecter' }">
								<input type="button" smt="dateSelecter" name="nodeEntity.${field.fieldName }_dateSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='timeSelecter' }">
								<input type="button" smt="timeSelecter" name="nodeEntity.${field.fieldName }_timeSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='dateTimeSelecter' }">
								<input type="button" smt="dateTimeSelecter" name="nodeEntity.${field.fieldName }_dateTimeSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='colorSelecter' }">
								<input type="button" smt="colorSelecter" name="nodeEntity.${field.fieldName }_colorSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags!='none' && field.assistTags!='' && field.displayByAddMode==true  }"></span></c:if>
						</td>
						
					</tr>
					</c:if>
				</c:forEach>
 	  	</table>
    </div>
    <div title="模板信息" class="hidden20150323">
 	  	<table cellpadding="0" cellspacing="0" border="0">
				<c:forEach items="${template }" var="field">
				<c:if test="${field.displayByAddMode==true }">
					<tr <c:if test="${field.displayByAddMode==false }">style="display:none;"</c:if>>
						<td class="tit" <c:if test="${field.displayByAddMode==false }">style="display:none;"</c:if>><div class="tittext">${field.displayName }</div></td>
						<td class="val">
							
							<c:if test="${field.tagType=='0' && field.displayByAddMode==true }">
							<span class="int"><input type="text" mthod="smt" name="nodeEntity.${field.fieldName }" value="${field.defaultValue }" <c:if test="${field.writeByAddMode==false }">readonly disabled</c:if> <c:if test="${field.displayByAddMode==false }">style="display:none;"</c:if><c:if test="${field.displayByAddMode==true && field.writeByAddMode==true && field.isRequired==true && (field.expGroup=='' || field.expGroup==null) }"> class="easyui-validatebox" data-options="required:true"</c:if>
<c:if test="${field.displayByAddMode==true && field.writeByAddMode==true && field.isRequired==true && field.expGroup!='' }"> class="easyui-validatebox" data-options="required:true,validType:'${field.expGroup}'"</c:if>
<c:if test="${field.displayByAddMode==true && field.writeByAddMode==true && field.isRequired==false && field.expGroup!='' }"> class="easyui-validatebox" data-options="validType:'${field.expGroup}'"</c:if>/></span>
							</c:if>
							<c:if test="${field.tagType=='1' && field.displayByAddMode==true }">
							<span class="int"><input type="text" mthod="smt" name="nodeEntity.${field.fieldName }" value="${field.defaultValue }" <c:if test="${field.writeByAddMode==false }">readonly disabled</c:if> <c:if test="${field.displayByAddMode==false }">style="display:none;"</c:if><c:if test="${field.displayByAddMode==true && field.writeByAddMode==true && field.isRequired==true && (field.expGroup=='' || field.expGroup==null) }"> class="easyui-validatebox" data-options="required:true"</c:if>
<c:if test="${field.displayByAddMode==true && field.writeByAddMode==true && field.isRequired==true && field.expGroup!='' }"> class="easyui-validatebox" data-options="required:true,validType:'${field.expGroup}'"</c:if>
<c:if test="${field.displayByAddMode==true && field.writeByAddMode==true && field.isRequired==false && field.expGroup!='' }"> class="easyui-validatebox" data-options="validType:'${field.expGroup}'"</c:if>/></span>
							</c:if>
							<c:if test="${field.tagType=='2' && field.displayByAddMode==true }">
							<span class="int"><textarea mthod="smt" name="nodeEntity.${field.fieldName }" <c:if test="${field.writeByAddMode==false }">readonly disabled</c:if> <c:if test="${field.displayByAddMode==false }">style="display:none;"</c:if><c:if test="${field.displayByAddMode==true && field.writeByAddMode==true && field.isRequired==true && (field.expGroup=='' || field.expGroup==null) }"> class="easyui-validatebox" data-options="required:true"</c:if>
<c:if test="${field.displayByAddMode==true && field.writeByAddMode==true && field.isRequired==true && field.expGroup!='' }"> class="easyui-validatebox" data-options="required:true,validType:'${field.expGroup}'"</c:if>
<c:if test="${field.displayByAddMode==true && field.writeByAddMode==true && field.isRequired==false && field.expGroup!='' }"> class="easyui-validatebox" data-options="validType:'${field.expGroup}'"</c:if>>${field.defaultValue }</textarea></span>
							</c:if>
							<c:if test="${field.tagType=='3' && field.displayByAddMode==true }">
							<span class="int">
							<select mthod="smt" name="nodeEntity.${field.fieldName }" <c:if test="${field.writeByAddMode==false }">readonly disabled</c:if> <c:if test="${field.displayByAddMode==false }">style="display:none;"</c:if>>
							<c:set value="${ fn:split(field.optionalValue, ';') }" var="values" />
								<c:forEach items="${values }" var="value">
									<c:set value="${ fn:split(value, ':') }" var="v" />
										<option value="${v[0] }" <c:if test="${v[0]==field.defaultValue }">selected</c:if>>${v[1] }</option>
								</c:forEach>
							</select>
							</span>
							</c:if>
							<c:if test="${field.tagType=='4' && field.displayByAddMode==true }">
							<span class="int">
							<select mthod="smt" name="nodeEntity.${field.fieldName }" <c:if test="${field.writeByAddMode==false }">readonly disabled</c:if> <c:if test="${field.displayByAddMode==false }">style="display:none;"</c:if>>
							<c:set value="${ fn:split(field.optionalValue, ';') }" var="values" />
								<c:forEach items="${values }" var="value">
									<c:set value="${ fn:split(value, ':') }" var="v" />
										<option value="${v[0] }" <c:if test="${v[0]==field.defaultValue }">selected</c:if>>${v[1] }</option>
								</c:forEach>
							</select>
							</span>
							</c:if>
							<c:if test="${field.tagType=='5' && field.displayByAddMode==true }">
							<span class="int">
							<select mthod="smt" name="nodeEntity.${field.fieldName }" <c:if test="${field.writeByAddMode==false }">readonly disabled</c:if> <c:if test="${field.displayByAddMode==false }">style="display:none;"</c:if>>
							<c:set value="${ fn:split(field.optionalValue, ';') }" var="values" />
								<c:forEach items="${values }" var="value">
									<c:set value="${ fn:split(value, ':') }" var="v" />
										<option value="${v[0] }" <c:if test="${v[0]==field.defaultValue }">selected</c:if>>${v[1] }</option>
								</c:forEach>
							</select>
							</span>
							</c:if>
							</span>
							<!--辅助控件部分-->
							<c:if test="${field.assistTags!='none' && field.assistTags!='' && field.displayByAddMode==true  }"><span class="assist"></c:if>
							<c:if test="${field.assistTags=='formatter' }">
								<input type="button" smt="formatter" name="nodeEntity.${field.fieldName }_formatter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='fileSelecter' }">
								<input type="button" smt="fileSelecter" name="nodeEntity.${field.fieldName }_fileSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='userSelecter' }">
								<input type="button" smt="userSelecter" name="nodeEntity.${field.fieldName }_userSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='templateSelecter' }">
								<input type="button" smt="templateSelecter" name="nodeEntity.${field.fieldName }_templateSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='dateSelecter' }">
								<input type="button" smt="dateSelecter" name="nodeEntity.${field.fieldName }_dateSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='timeSelecter' }">
								<input type="button" smt="timeSelecter" name="nodeEntity.${field.fieldName }_timeSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='dateTimeSelecter' }">
								<input type="button" smt="dateTimeSelecter" name="nodeEntity.${field.fieldName }_dateTimeSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='colorSelecter' }">
								<input type="button" smt="colorSelecter" name="nodeEntity.${field.fieldName }_colorSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags!='none' && field.assistTags!='' && field.displayByAddMode==true  }"></span></c:if>
						</td>
						
					</tr>
					</c:if>
				</c:forEach>
					</tr>
						<td class="tit"><div class="tittext">模板库ID</div></th>
						<td class="val">
							<span class="int">
							<select mthod="smt" name="attrEntity.templateGroupId">
								<option value="-1" <c:if test="${attrEntity.templateGroupId==-1 }">selected</c:if>>使用上级设置</option>
							</select>
							</span>
						</td>
					</tr>
					</tr>
						<td class="tit"><div class="tittext">栏目页面模板设置</div></th>
						<td class="val">
							<span class="int">
							<select mthod="smt" name="attrEntity.nodePageTemplateId">
								<option value="-1" <c:if test="${attrEntity.nodePageTemplateId==-1 }">selected</c:if>>使用上级设置</option>
							</select>
							</span>
						</td>
					</tr>
					</tr>
						<td class="tit"><div class="tittext">栏目样式模板设置</div></th>
						<td class="val">
							<span class="int">
							<select mthod="smt" name="attrEntity.nodeStyleTemplateId">
								<option value="-1" <c:if test="${attrEntity.nodeStyleTemplateId==-1 }">selected</c:if>>使用上级设置</option>
							</select>
							</span>
						</td>
					</tr>
					</tr>
						<td class="tit"><div class="tittext">内容页面模板设置</div></th>
						<td class="val">
							<span class="int">
							<select mthod="smt" name="attrEntity.contentpageTemplateId">
								<option value="-1" <c:if test="${attrEntity.contentpageTemplateId==-1 }">selected</c:if>>使用上级设置</option>
							</select>
							</span>
						</td>
					</tr>
					</tr>
						<td class="tit"><div class="tittext">内容样式模板设置</div></th>
						<td class="val">
							<span class="int">
							<select mthod="smt" name="attrEntity.contentStyleTemplateId">
								<option value="-1" <c:if test="${attrEntity.contentStyleTemplateId==-1 }">selected</c:if>>使用上级设置</option>
							</select>
							</span>
						</td>
					</tr>
					</tr>
						<td class="tit"><div class="tittext">评论页面模板设置</div></th>
						<td class="val">
							<span class="int">
							<select mthod="smt" name="attrEntity.commentpageTemplateId">
								<option value="-1" <c:if test="${attrEntity.commentpageTemplateId==-1 }">selected</c:if>>使用上级设置</option>
							</select>
							</span>
						</td>
					</tr>
					</tr>
						<td class="tit"><div class="tittext">评论样式模板设置</div></th>
						<td class="val">
							<span class="int">
							<select mthod="smt" name="attrEntity.commentStyleTemplateId">
								<option value="-1" <c:if test="${attrEntity.commentStyleTemplateId==-1 }">selected</c:if>>使用上级设置</option>
							</select>
							</span>
						</td>
					</tr>
		</table>
    </div>
    </div>
    </div>
    </div>
    </body>
    <script type="text/javascript">
    var path='<%=path%>';
    $(document).ready(function(){
		bindEvents();
	});
	function bindEvents(){
		$("input[smt='formatter']").click(function(e){parent.showFormatterDialog(e);});
		$("input[smt='fileSelecter']").click(function(e){selectFile(e,'${parentId}');});
		$("input[smt='userSelecter']").click(function(e){parent.showUserSelecterDialog(e);});
		$("input[smt='templateSelecter']").click(function(e){parent.showTemplateSelecter(e);});
		$("input[smt='dateSelecter']").click(function(e){});//不需要上级页面帮忙完成显示
		$("input[smt='timeSelecter']").click(function(e){});//不需要上级页面帮忙完成显示
		$("input[smt='dateTimeSelecter']").click(function(e){});//不需要上级页面帮忙完成显示
		$("input[smt='colorSelecter']").click(function(e){});//不需要上级页面帮忙完成显示
	};	
	var curImageId="";
	function selectFile(e,nodeId){
		curImageId=$(e.target).attr("name").split("_")[0];
		window.open(path+'/Manage/FileSelecter/index.do?nodeId='+nodeId+'&filter='+curImageId,'文件上传','toolbar=no,menubar=no,resizable=no,location=no,status=no,z-look:yes');
	};
	function changeFile(s,f){
		if(s!=""){
			s=s.substring(0,s.length-1);
		}
		if($("input[name='"+curImageId+"_fileAppend']:checked").length>0){
			$("input[name='"+f+"']").val($("input[name='"+f+"']").val()+";"+s);
		}else{
			$("input[name='"+f+"']").val(s);
		}
       
	};

	function setValue(name,val){
		$("[name='"+name+"']").val(val);
		$("[name='"+name+"']").blur();
	};
	
	String.prototype.replaceAll = function(s1,s2) { 
    	return this.replace(new RegExp(s1,"gm"),s2); 
	};
	var rtn=false;
	function submit(){
		var _s_tmp=$("[data-options]");
		var flog=true;
		for(i=0;i<_s_tmp.size();i++){
			if(!$(_s_tmp[i]).validatebox("isValid")){
				flog=false;
				break;
			}
		} 
		if(!flog){
			return false;
		}
		
		if($("#parentId").val()==""){
			alert("参数错误");
			return false;
		}
		
		
		var _s_tmp=$("[mthod='smt']");
		var params="";
		for(i=0;i<_s_tmp.size();i++){
			params+="'"+(_s_tmp[i].name)+"':'"+$(_s_tmp[i]).val()+"',";
		//	if(_s_tmp[i].tagName=="SELECT"){
		//		params+="'"+(_s_tmp[i].name)+"':'"+$(_s_tmp[i]).val()+"',";//.selectedOptions[0].value+"',";
		//	}else if(_s_tmp[i].tagName=="INPUT"){
		//		params+="'"+(_s_tmp[i].name)+"':'"+$(_s_tmp[i]).val()+"',";
		//	}else{
		//	}
		} 
		params="[{"+params+"'nodeEntity.parentId':"+$("#parentId").val()+"}]";
		
			$.ajax({
				 type: "POST",
				 url: path+"/Manage/Node/doAdd.do?t="+Math.ceil(Math.random()*999999),
				 data: eval(params)[0],
				 dataType: "text",
				 async:false,
				 cache:false,
				 success: function(result){
					if(result.indexOf("msg")!=0){
						var t=eval("("+result+")");
						if(t.status='99'){
							rtn=true;
						}else{
							alert(t.msg);
						}
					}
				}
			 });
		return rtn;
	}
</script>