<%@ page language="java" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort() + path + "/";

	StringBuffer uploadUrl = new StringBuffer("http://");
	uploadUrl.append(request.getHeader("Host"));
	uploadUrl.append(request.getContextPath());
	uploadUrl.append("/manage/crm/doUploadByBaseBarCode.do");
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		
		<title>带条码的控制信息单文件</title>
		<link href="<%=basePath%>css/fileupload.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="<%=basePath%>css/button.css" type="text/css" />
		
		<link rel="stylesheet" type="text/css" href="<%=basePath%>js/ext/resources/css/ext-all.css" />
 		<script type="text/javascript" src="<%=basePath%>js/ext/adapter/ext/ext-base.js"></script>
    	<script type="text/javascript" src="<%=basePath%>js/ext/ext-all.js"></script>
  		<script type="text/javascript" src="<%=basePath%>js/ext/ext-lang-zh_CN.js"></script>
		
		<script type="text/javascript" src="<%=basePath%>js/swfupload/swfupload.js"></script>
		<script type="text/javascript" src="<%=basePath%>js/swfupload/swfupload.queue.js"></script>
		<script type="text/javascript" src="<%=basePath%>js/swfupload/handlers.js"></script>

		<script type="text/javascript" src="<%=basePath%>js/swfupload/swfupload.js"></script>
		<script type="text/javascript" src="<%=basePath%>js/swfupload/handlers.js"></script>
		<script type="text/javascript">
			var swfu;
			window.onload = function () {
				swfu = new SWFUpload({
					upload_url: "<%=uploadUrl.toString()%>",
					post_params: {},//"uCode" : getUCode(),"split" : "false"
					use_query_string : true,
					// File Upload Settings
					file_size_limit : "3 MB",	// 1000MB
					file_types : "*.txt",
					file_types_description:"目前仅仅可以导入规定格式的txt文件",
					file_types_description : "文本文件",
					file_upload_limit : "0",
					prevent_swf_caching:true,
					file_queue_error_handler : fileQueueError,
					file_dialog_complete_handler : fileDialogComplete,//选择好文件后提交
					file_queued_handler : fileQueued,
					upload_progress_handler : uploadProgress,
					upload_error_handler : uploadError,
					upload_success_handler : uploadSuccess,
					upload_complete_handler : uploadComplete,
					file_upload_limit:1,
					// Button Settings
					button_image_url : "<%=basePath%>images/SmallSpyGlassWithTransperancy_17x18.png",
					button_placeholder_id : "spanButtonPlaceholder",
					button_width: 198,
					button_height: 18,
					button_text : '<span class="button">选择文件 <span class="buttonSmall">(最大3MB)</span></span>',
					button_text_style : '.button { font-family: Helvetica, Arial, sans-serif; font-size: 12px; } .buttonSmall { font-size: 10pt; }',
					button_text_top_padding: 0,
					button_text_left_padding: 18,
					button_window_mode: SWFUpload.WINDOW_MODE.TRANSPARENT,
					button_cursor: SWFUpload.CURSOR.HAND,
					
					// Flash Settings
					flash_url : "<%=basePath%>js/swfupload/swfupload.swf",
	
					custom_settings : {
						upload_target : "divFileProgressContainer"
					},
					// Debug Settings
					debug: false  //是否显示调试窗口
				});
			};
			function startUploadFile(){
				swfu.startUpload();
			};
			function showExtShow(){
				win.show();
			};

		</script>
	</head>
	<body style="background-color: #C0D1E3; padding: 2px;">
		<div id="content" style="float:left;margin:0;padding:0;width:100%;margin-top:20px;">
			<form>
				<div
					style="float:left;width:200px;height:20px;display: inline; border: solid 1px #7FAAFF; background-color: #C5D9FF; padding: 2px;">
					<span id="spanButtonPlaceholder"></span>
				</div>
				<div style="float:left;width:170px;height:20px;">
							<input id="btnUpload" type="button" value="上  传"
						onclick="startUploadFile();" class="btn3_mouseout" onMouseUp="this.className='btn3_mouseup'"
						onmousedown="this.className='btn3_mousedown'"
						onMouseOver="this.className='btn3_mouseover'"
						onmouseout="this.className='btn3_mouseout'"/>
					<input id="btnCancel" type="button" value="取消所有上传"
						onclick="cancelUpload();" disabled="disabled" class="btn3_mouseout" onMouseUp="this.className='btn3_mouseup'"
						onmousedown="this.className='btn3_mousedown'"
						onMouseOver="this.className='btn3_mouseover'"
						onmouseout="this.className='btn3_mouseout'"/>
				</div>
			</form>
			<div style="float:left;width:400px;" id="divFileProgressContainer"></div>
			<div style="float:left;width:400px;" id="thumbnails">
				<table id="infoTable" border="0" width="400" style="display: inline; border: solid 1px #7FAAFF; background-color: #C5D9FF; padding: 2px;margin-top:8px;">
				</table>
			</div>
		</div>
	
	</body>
	<script type="text/javascript">
	
	</script>
</html>